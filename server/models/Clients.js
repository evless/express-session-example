const config = require('config');
const mongoose = require('mongoose');

let clients = new mongoose.Schema({
    code: String,
    token: String
});

module.exports = mongoose.model('Clients', clients);