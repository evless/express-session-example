const express = require('express');
const router = express.Router();

router.get('/test', function(req, res) {
    res.send(`Server is working! logined: ${req.isAuthenticated()};`);
});

router.get('/login', function(req, res) {
    res.send('Вы не залогинены! А НУ БЫСТРО!');
});

module.exports = router;