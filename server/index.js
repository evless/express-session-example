const config = require('config');
const path = require('path');
const fs = require('fs');
const thunkify = require('thunkify');
const read = thunkify(fs.readFile);

const app = require('express')();

// Коннект к базе
const mongoose = require('mongoose');
mongoose.connect(`mongodb://localhost/express-session`);

// Распаковка мидлваров
const middlewares = fs.readdirSync(path.join(__dirname, 'middlewares')).sort();
middlewares.forEach(function(middleware) {
    app.use(require('./middlewares/' + middleware));
});

// Распаковка роутинга
const routers = fs.readdirSync(path.join(__dirname, 'routers')).sort();
routers.forEach(function(routers) {
    app.use(require('./routers/' + routers));
});

// Запуск сервера
app.listen(config.port);

