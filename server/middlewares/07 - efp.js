const passport = require('passport');
const Clients = require('../models/Clients');

module.exports = function(req, res, done) {
    if (req.isAuthenticated()) {
        done()
    } else {
        if (req.query.code) {
            // Тут мы типа обрамащаемся за получением токена
            // Тут типа получили токен
            // На основе кода и токена создаем юзера
            let token = 'NJacb23d';

            let client = new Clients({
                code: req.query.code,
                token: token
            })
            
            client.save((err, data) => {
                if (!err) {
                    req.body = data;
                    passport.authenticate('local', function(err, user, info) {
                        if (err) throw err
                        if (user === false) {
                            res.status = 401;
                            res.send({
                                status: false,
                                message: info.message
                            });
                        } else {
                            req.logIn(user, function(err) {
                                if (err) { return next(err); }
                                done();
                            });
                        }
                    }).call(this, req, res, done);
                }
            });
        } else {
            res.redirect('http://ya.ru');
        }
    }
}