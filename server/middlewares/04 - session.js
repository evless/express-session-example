const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);

const store = new MongoDBStore({
    uri: `mongodb://localhost/express-session`,
    collection: 'sessions'
});

module.exports = require('express-session')({
    secret: 'This is a secret',
    cookie: {
        maxAge: 3000
    },
    store: store,
    resave: true,
    saveUninitialized: true
});

