const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const Clients = require('../models/Clients');

passport.serializeUser(function(client, done) {
    console.log('serializeUser')
    done(null, client); // uses _id as idFieldd
});

passport.deserializeUser(function(id, done) {
    console.log('deserializeUser', id)
    Clients.findById(id, done); // callback version checks id validity automatically
});

// done(null, user)
// OR
// done(null, false, { message: <error message> })  <- 3rd arg format is from built-in messages of strategies
passport.use(new LocalStrategy({
        usernameField: 'code', // 'username' by default
        passwordField: 'token'
    },
    function(code, token, done) {
        console.log('code, token')
        console.log(code, token)
        Clients.findOne({ code: code }, function (err, client) {
            console.log('err, client')
            console.log(err, client)
            if (err) {
                return done(err);
            }
            if (!client) {
                // don't say whether the user exists
                return done(null, false, { message: 'Нет такого пользователя или пароль неверен.' });
            }
            return done(null, client._id);
        });
    }
));

module.exports = passport.initialize();